export interface IBurgerInterface {
    burgerData: {
        purchasing : Boolean,
        ingredients : Object,
        totalPrice : Number,
        INGREDIENT_PRICE : Object,
        purchasable : Boolean,
        _id : String,
        burgerData: any
    },
    purschaseCancelHandler: Function,
    addIngredientHandler: Function,
    removeIngredienthandler: Function,
    purchaseHandler : Function,
    fetchAllIngredientHandler: Function 
}