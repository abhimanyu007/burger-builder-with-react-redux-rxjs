export const ADD_INGREDIENT = 'ADD_INGREDIENT';
export const REMOVE_INGREDIENT = 'REMOVE_INGREDIENT';
export const ORDER = 'ORDER';
export const CANCEL_ORDER = 'CANCEL_ORDER';
export const FETCH_ALLINGREDIENT = 'FETCH_ALLINGREDIENT';

export const fetchIngredientAction = () => ({
    type: FETCH_ALLINGREDIENT,
});

export const addIngredientAction = () => ({
    type: ADD_INGREDIENT,
});

export const removeIngredientAction = (whiskies) => ({
    type: REMOVE_INGREDIENT,
    payload: whiskies
});

export const orderAction = (message) => ({
    type: ORDER,
    payload: message
});

export const cancelOrderAction = (message) => ({
    type: CANCEL_ORDER,
    payload: message
});