import React, { Component } from 'react';

import Aux from '../../hoc/_Aux';
import Burger from '../../components/Burger/Burger';
import BuildControls from "../../components/Burger/BuildControls/BuildControls";
import Modal from '../../components/UI/Modal/Modal'
import OrderSummary from '../../components/Burger/OrderSummary/OrderSummary';
import { connect } from "react-redux";
import { IBurgerInterface } from '../../interfaces/burger.interface';

class BurgerBuilder extends Component<IBurgerInterface, {}> {

    componentDidMount() {
        this.props.fetchAllIngredientHandler();
    }
    render () {
        return (
            <Aux>
                <Modal show={this.props.burgerData.purchasing} onModelClose = {this.props.purschaseCancelHandler}>
                    <OrderSummary 
                        ingredients={this.props.burgerData.ingredients} 
                        totalPrice={this.props.burgerData.totalPrice}
                        allIngredientPrice = {this.props.burgerData.INGREDIENT_PRICE}
                        onCancelOrder = {this.props.purschaseCancelHandler} />
                </Modal>
                <Burger ingredients={this.props.burgerData.ingredients} />
                <BuildControls 
                    purchasable={this.props.burgerData.purchasable} 
                    ingredients={this.props.burgerData.ingredients} 
                    totalPrice={this.props.burgerData.totalPrice} 
                    ingredientsAdded={this.props.addIngredientHandler} 
                    ingredientsRemoved={this.props.removeIngredienthandler}
                    ordered={this.props.purchaseHandler}
                    id= {this.props.burgerData._id}
                    burgerData= {this.props.burgerData}/>
            </Aux>
        );
    }
}

const mapStateToProps = state => {
    return {
        burgerData: state.burger_reducer
    };
};

const mapDispatchToProps = dispatch => {
    return {
        fetchAllIngredientHandler:  () => dispatch({type: 'FETCH_ALLINGREDIENT'}),
        addIngredientHandler: (ingredientType, id, data) => {dispatch({type: 'ADD_INGREDIENT', ingredientType, id, data})},
        removeIngredienthandler: (ingredientType, id, data) => dispatch({type: 'REMOVE_INGREDIENT', ingredientType, id, data}),
        purchaseHandler: () => dispatch({type: 'ORDER'}),
        purschaseCancelHandler: () => dispatch({type: 'CANCEL_ORDER'})
    };
};

export default connect(mapStateToProps,mapDispatchToProps)(BurgerBuilder);