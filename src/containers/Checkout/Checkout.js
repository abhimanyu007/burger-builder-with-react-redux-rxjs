import React, { Component } from 'react'
import { connect } from "react-redux";
import CounterControl from '../../components/CounterControl/CounterControl';
import CounterOutput from '../../components/CounterOutput/CounterOutput';
import {Table} from 'react-bootstrap';
class Checkout extends Component {

    render () {
        const ingredientsSummary = Object.keys(this.props.burgerData.ingredients).map((igKey, index) => {
            return this.props.burgerData.ingredients[igKey] > 0 ? (
                        <tr key={igKey}>
                            <td>{index + 1}</td>
                            <td>{igKey}</td>
                            <td>{this.props.burgerData.ingredients[igKey]}</td>
                            <td>{this.props.burgerData.ingredients[igKey] * this.props.burgerData.INGREDIENT_PRICE[igKey]}</td>
                        </tr>
                    ) : null
        });
        return (
            <div>
                <Table striped bordered hover>
                    <thead>
                        <tr>
                        <th>#</th>
                        <th>Items</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {ingredientsSummary}
                        <tr variant="secondary">
                            <td colSpan="3"><strong>Total</strong></td>
                            <td><strong>{this.props.burgerData.totalPrice.toFixed(2)}</strong></td>
                        </tr>
                    </tbody>
                </Table>
                <CounterOutput value={this.props.counter} />
                <CounterControl label="Increment" clicked={this.props.onIncrementCounter} />
                <CounterControl label="Decrement" clicked={this.props.onDcreamentCounter}  />
                <CounterControl label="Add 5" clicked={this.props.addFiveToCounter}  />
                <CounterControl label="Subtract 5" clicked={this.props.subtractFiveToCounter}  />
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        counter: state.checkout_reducer.counter,
        burgerData: state.burger_reducer
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onIncrementCounter: () => dispatch({type: 'INCREMENT'}),
        onDcreamentCounter: () => dispatch({type: 'DECREMENT'}),
        addFiveToCounter: () => dispatch({type: 'ADD', value: 5}),
        subtractFiveToCounter: () => dispatch({type: 'SUBTRACT', value: 5})
    };
};

export default connect(mapStateToProps,mapDispatchToProps)(Checkout);
