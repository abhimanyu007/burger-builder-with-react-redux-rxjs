import { combineEpics, ofType } from 'redux-observable';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/catch';
import { map, flatMap } from 'rxjs/operators'
import { ajax } from 'rxjs/observable/dom/ajax';



const getUrl = 'http://localhost:8000/getAllIngredients'; // The API for the GET INGREDIENTS
const addUrl = 'http://localhost:8000/addIngredient'; // The API for the GET INGREDIENTS
/*
    The API returns the data in the following format:
    {
        "count": number,
        "next": "url to next page",
        "previous": "url to previous page",
        "results: array of whiskies
    }
    since we are only interested in the results array we will have to use map on our observable
 */
const fetchInitialData = action$ => action$.pipe(
    ofType('FETCH_ALLINGREDIENT'),
    flatMap(() => ajax.getJSON(getUrl)),
    map(user => ({ type: 'FETCH_ALLINGREDIENT_SUCCESS', payload: user[0] }))
)
const addIngredientsInBurger = action$ => action$.pipe(
    ofType('ADD_INGREDIENT'),
    flatMap(action => { 
        const actionString = JSON.stringify(action);
        return ajax({
        url: addUrl,
        method: 'PUT',
        body: {action : actionString, callType: 'add' }
    })}),
    map(data => ({ type: 'ADDED_INGREDIENT_SUCCESS', payload: data.response[0] }))
)

const removeIngredientsFromBurger = action$ => action$.pipe(
    ofType('REMOVE_INGREDIENT'),
    flatMap(action => { 
        const actionString = JSON.stringify(action);
        return ajax({
        url: addUrl,
        method: 'PUT',
        body: {action : actionString, callType: 'remove' }
    })}),
    map(data => ({ type: 'REMOVED_INGREDIENT_SUCCESS', payload: data.response[0] }))
)
const rootEpic = combineEpics(fetchInitialData, addIngredientsInBurger, removeIngredientsFromBurger);
export default  rootEpic;