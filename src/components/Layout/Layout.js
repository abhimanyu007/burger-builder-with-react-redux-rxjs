import React from 'react';
import { BrowserRouter} from 'react-router-dom'
import Aux from '../../hoc/_Aux';
import classes from './Layout.css';
import Toolbar from '../Navigation/Toolbar/Toolbar';
const layout = ( props ) => (
    <Aux>
        <BrowserRouter>
            <Toolbar/ >
            <main className={classes.Content}>
                {props.children}
            </main>
        </BrowserRouter>
    </Aux>
);

export default layout;