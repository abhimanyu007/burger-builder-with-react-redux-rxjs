import React, { Fragment } from 'react';
import {Image} from 'react-bootstrap';
import burgerLogo from '../../assests/images/burger-logo.png';
import classes from './Logo.css'
export default function Logo(props) {
    return (
        <Fragment>
            <Image width="40" height="30" src={burgerLogo} fluid className={`d-inline-block align-top ${classes.Logo}`}/>
        </Fragment>    
    )
}
