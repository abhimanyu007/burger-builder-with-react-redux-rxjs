import React from 'react';
import Aux from '../../../hoc/_Aux';
import classes from './OrderSummary.css'
import { Button, Table, Badge } from "react-bootstrap";
import { Link } from 'react-router-dom'
const orderSummary = (props) => {
    const ingredientsSummary = Object.keys(props.ingredients).map((igKey, index) => {
        return props.ingredients[igKey] > 0 ? (
                    <tr key={igKey}>
                        <td>{index + 1}</td>
                        <td>{igKey}</td>
                        <td>{props.ingredients[igKey]}</td>
                        <td>{props.ingredients[igKey] * props.allIngredientPrice[igKey]}</td>
                    </tr>
                ) : null
    });

    return (
        <Aux>
            <div className={classes.OrderSummary}>
                <h3 variant="success">Your order</h3>
                <p>A delicious burger with following Ingredients :</p>
                <Table striped bordered hover>
                    <thead>
                        <tr>
                        <th>#</th>
                        <th>Items</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {ingredientsSummary}
                        <tr variant="secondary">
                            <td colSpan="3"><strong>Total</strong></td>
                            <td><strong>{props.totalPrice.toFixed(2)}</strong></td>
                        </tr>
                    </tbody>
                </Table>
                <Badge variant="secondary">Contine to Checkout ?</Badge><br></br>
                <Link to='/checkout'>
                    <Button variant="success" onClick={props.onContinueCheckout}>Continue</Button>
                </Link>
                <Button variant="warning" onClick={props.onCancelOrder}>Cancel</Button>
            </div>
        </Aux>
    )
}

export default orderSummary;