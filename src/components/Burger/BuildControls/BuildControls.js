import React from "react";

import  classes  from "./BuildControls.css";
import BuildControl from "./BuildControl/BuildControl";

const controls = [
    {label:'Salad', type:'salad'},
    {label:'Bacon', type:'bacon'},
    {label:'Cheese', type:'cheese'},
    {label:'Meat', type:'meat'}
]

const buildControls = (props) => (
    <div className={classes.BuildControls}>
        <p>Total Price : <strong> {props.totalPrice} </strong></p>
        {controls.map( ctrl => (
            <BuildControl 
                key={ctrl.type} 
                type={ctrl.type}
                label={ctrl.label}
                added={() => props.ingredientsAdded(ctrl.type, props.id, props.burgerData)}
                removed={() => props.ingredientsRemoved(ctrl.type, props.id, props.burgerData)}
                ingredients={props.ingredients}
            />
        ))}
        <button 
            className={classes.OrderButton}
            disabled={!props.purchasable}
            onClick={props.ordered}>ORDER NOW</button>
    </div>
)

export default buildControls;