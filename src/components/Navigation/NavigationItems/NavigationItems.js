import React, { Fragment } from 'react';
import { Nav, NavDropdown } from 'react-bootstrap';
import classes from './NavigationItems.css';
import { Link } from 'react-router-dom'
export default function NavigationItems(props) {
    return (
        <Fragment >
          <Nav className={`mr-auto ${classes.NavigationItems}`}>
            <Nav.Link href="#features">Features</Nav.Link>
            <Nav.Link href="#pricing">Pricing</Nav.Link>
            <NavDropdown title="Dropdown" className={classes.navDropdown}  id="collasible-nav-dropdown">
                <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
                <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
                <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
            </NavDropdown>
            </Nav>
            <Nav className={classes.NavigationItems}>
              <Link to="/burger-builder">
                  Burger
              </Link>
              <Link eventkey={2} to="/checkout">
                  Checkout
              </Link>
          </Nav>
        </Fragment>
    )
}
