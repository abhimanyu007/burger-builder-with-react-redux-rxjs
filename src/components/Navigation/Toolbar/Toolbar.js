import React from 'react';
import classes from './Toolbar.css'
import Logo from "../../Logo/Logo";
import NavigationItems from "../NavigationItems/NavigationItems";
import { Row, Navbar } from 'react-bootstrap';
const Toolbar = () => {
    return (
        <header>
            <Row> 
                <Navbar collapseOnSelect expand="lg" variant="dark" className={`col-md-12 ${classes.Toolbar}`}>
                    <Navbar.Brand href="#home">
                        <Logo/>
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav">
                        <NavigationItems  active/>
                    </Navbar.Collapse>
                </Navbar>
            </Row>
        </header>
    )
}

export default Toolbar;
