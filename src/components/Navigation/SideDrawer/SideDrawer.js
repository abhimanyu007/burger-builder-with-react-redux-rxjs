import React from 'react'
import Logo from "../../Logo/Logo";
import Navigationitems from '../NavigationItems/NavigationItems';
import classes from './SideDrawer.css';
import { Button, Row } from 'react-bootstrap';

export default function SideDrawer(props) {
    return (
        <div className={classes.SideDrawer}>
            <Row>
                <div className={classes.Logo}>
                    <Logo/>
                </div>
            </Row>
            <Row className='d-md-none d-lg-none'>
                <Navigationitems  desktop={false}/>
            </Row>
        </div>
    )
}
