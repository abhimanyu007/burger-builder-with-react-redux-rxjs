import { createStore , combineReducers, applyMiddleware} from 'redux'
// import { persistStore, persistReducer, persistCombineReducers } from 'redux-persist'
// import storage from 'redux-persist/lib/storage' // defaults to localStorage for web
import { createEpicMiddleware } from 'redux-observable';
import checkout_reducer from './reducers/checkout_reducer';
import burger_reducer from './reducers/burger_reducer';
import rootEpic  from './epics/epics';
/* const persistConfig = {
  key: 'root',
  storage,
} */
// const persistedReducer = persistCombineReducers(persistConfig, {checkout_reducer, burger_reducer})
const epicMiddleware = createEpicMiddleware();
const combineReducer = combineReducers({checkout_reducer, burger_reducer})
const store = createStore(combineReducer, applyMiddleware(epicMiddleware))
epicMiddleware.run(rootEpic);
// const persistor = persistStore(store)
export default store;