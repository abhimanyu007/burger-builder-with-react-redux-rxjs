import { ADD_INGREDIENT,
        REMOVE_INGREDIENT,
        ORDER,
        CANCEL_ORDER, FETCH_ALLINGREDIENT } from "../actions/burger_actions";

const initialState = {
    ingredients: {},
    totalPrice: 4,
    purchasable: false,
    purchasing: false,
    INGREDIENT_PRICE : {}
}

const purchaseHandler = (state) => {
    return {...state, purchasing: true}
}

const purschaseCancelHandler = (state) => {
    return {...state, purchasing: false}
}

const burger_reducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_INGREDIENT:
            return state;
        
        case 'ADDED_INGREDIENT_SUCCESS':
            return {
                ...action.payload
            }
            
        case REMOVE_INGREDIENT:
            return state;
            
        case 'REMOVED_INGREDIENT_SUCCESS':
            return {
                ...action.payload
            }

        case FETCH_ALLINGREDIENT:
            return state;

        case 'FETCH_ALLINGREDIENT_SUCCESS':
            return {
                ...action.payload
            }
        case ORDER:
            return purchaseHandler(state);

        case CANCEL_ORDER:
            return purschaseCancelHandler(state);

        default:
            return state;
    }
};

export default burger_reducer;