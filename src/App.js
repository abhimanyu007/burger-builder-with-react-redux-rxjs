import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom'
import Layout from './components/Layout/Layout';
import BurgerBuilder from './containers/BurgerBuilder/BurgerBuilder';
import Checkout from './containers/Checkout/Checkout';
import Home from './containers/Home/Home';

class App extends Component {
  render () {
    return (
      <div className="container-fluid">
          <Layout>
            <Switch>
              <Route path="/" exact component={Home}/>
              <Route path="/burger-builder" component={BurgerBuilder}/>
              <Route path="/checkout" component={Checkout}/>
            </Switch>
          </Layout>
      </div>
    );
  }
}

export default App;
