import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import ReactDOM from 'react-dom';
import './index.css';
// import 'bootstrap/dist/css/bootstrap-theme.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import { Provider } from 'react-redux';
/* import thunk from 'redux-thunk'; */
import store from './storeConfig';

ReactDOM.render(
        <Provider store={store}>
                <App />
        </Provider>, 
        document.getElementById('root'));
registerServiceWorker();
